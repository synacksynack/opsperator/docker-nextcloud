#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh

NEXTCLOUD_DATA_DIR="${NEXTCLOUD_DATA_DIR:-/var/www/html/data}"

run_as()
{
    if test "`id -u`" = 0; then
	su - www-data -s /bin/sh -c "$1"
    else
	sh -c "$1"
    fi
}

if test "$NEXTCLOUD_CRON_INTERVAL" -ge 59 >/dev/null 2>&1; then
    while sleep $NEXTCLOUD_CRON_INTERVAL
    do
	echo == Executing Cron.php on `date +%s` ==
	run_as 'php -f /var/www/html/cron.php'
	echo == Cron.php Done on `date +%s` ==
    done >>"$NEXTCLOUD_DATA_DIR/nextcloud.log" 2>&1
fi
run_as 'php -f /var/www/html/cron.php' >>"$NEXTCLOUD_DATA_DIR/nextcloud.log" 2>&1

exit $?
