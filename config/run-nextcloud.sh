#!/bin/sh

if test "$DEBUG"; then
    set -x
    LOGLEVEL=2
else
    LOGLEVEL=1
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
DO_ADMINAUDIT=${DO_ADMINAUDIT:-true}
DO_PRIVACY=${DO_PRIVACY:-true}
DO_REASONS=${DO_REASONS:-true}
DO_SOCIALS=${DO_SOCIALS:-true}
ELASTICSEARCH_HOST=${ELASTICSEARCH_HOST:-elasticsearch}
ELASTICSEARCH_INDEX=${ELASTICSEARCH_INDEX:-nextcloud}
ELASTICSEARCH_PORT=${ELASTICSEARCH_PORT:-9200}
ELASTICSEARCH_PROTO=${ELASTICSEARCH_PROTO:-http}
LDAP_TLS_NO_VERIFY=1
LOOKUP_JWT_KEY=${LOOKUP_JWT_KEY:-secret}
LOOL_PORT=${LOOL_PORT:-9980}
LOOL_PROTO=${LOOL_PROTO:-http}
LOOL_VERSION=${LOOL_VERSION:-3.4.6}
LOOL_WS_PROTO=${LOOL_WS_PROTO:-ws}
MAIL_FROM_USER=${MAIL_FROM_USER:-nextcloud}
MAIL_SENDMAIL_MODE=${MAIL_SENDMAIL_MODE:-smtp}
MAIL_SMTP_MODE=${MAIL_SMTP_MODE:-smtp}
MATOMO_SITE_ID=${MATOMO_SITE_ID:-1}
MATOMO_TRACK_DIR=${MATOMO_TRACK_DIR:-true}
MATOMO_TRACK_USER=${MATOMO_TRACK_USER:-true}
NEXTCLOUD_ADMIN_PASSWORD=${NEXTCLOUD_ADMIN_PASSWORD:-secret}
NEXTCLOUD_ADMIN_USER=${NEXTCLOUD_ADMIN_USER:-admin}
NEXTCLOUD_CONFIGMAP_NAME=${NEXTCLOUD_CONFIGMAP_NAME:-nextcloud-config}
NEXTCLOUD_DATA_DIR="${NEXTCLOUD_DATA_DIR:-/var/www/html/data}"
NEXTCLOUD_DATA_LOCATION=${NEXTCLOUD_DATA_LOCATION:-}
NEXTCLOUD_GSS_USERMAPPING=${NEXTCLOUD_GSS_USERMAPPING:-/user-mapping.json}
NEXTCLOUD_K8S_NAMESPACE=${NEXTCLOUD_K8S_NAMESPACE:-}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=nextcloud,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_GROUP_FILTER="${OPENLDAP_GROUP_FILTER:-All}"
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
LLNG_PROTO=${LLNG_PROTO:-http}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
SAML_ADMIN_USER="${SAML_ADMIN_USER:-admin0}"
SMTP_PORT=${SMTP_PORT:-25}
WAS_INIT=false
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=cloud.$OPENLDAP_DOMAIN
fi
if test -z "$LLNG_SSO_DOMAIN"; then
    LLNG_SSO_DOMAIN=auth.$OPENLDAP_DOMAIN
fi
if test "$LOOL_ENDPOINT"; then
    if ! test "$LOOL_PROTO" = https -a "$LOOL_PORT" = 443; then
	if ! test "$LOOL_PROTO" = http -a "$LOOL_PORT" = 80; then
	    LOOL_ENDPOINT="$LOOL_ENDPOINT:$LOOL_PORT"
	fi
    fi
fi
if test -z "$NEXTCLOUD_GSS_MASTER"; then
    NEXTCLOUD_GSS_MASTER=$APACHE_DOMAIN
fi
if test -z "$PHP_MEMORY_LIMIT"; then
    PHP_MEMORY_LIMIT=-1
fi
if test "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" -a "$S3_HOST"; then
    S3_BUCKET=${S3_BUCKET:-nextcloud}
    S3_PORT=${S3_PORT:-8080}
    S3_REGION=${S3_REGION:-us-east-1}
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false
export APACHE_IGNORE_OPENLDAP=true

PHPCMD="php -d memory_limit=$PHP_MEMORY_LIMIT"

cpt=0
if test "$POSTGRES_PASSWORD"; then
    POSTGRES_DB=${POSTGRES_DB:-nextcloud}
    POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
    POSTGRES_PORT=${POSTGRES_PORT:-5432}
    POSTGRES_USER=${POSTGRES_USER:-nextcloud}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
elif test "$MYSQL_PASSWORD"; then
    MYSQL_DATABASE=${MYSQL_DATABASE:-nextcloud}
    MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
    MYSQL_PORT=${MYSQL_PORT:-3306}
    MYSQL_USER=${MYSQL_USER:-nextcloud}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
fi
if test "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" -a "$S3_HOST" -a \
	-x /usr/bin/s4cmd; then
    cpt=0
    echo Waiting for s3 backend ...
    sed -e "s|S3_ACCESS_KEY|$S3_ACCESS_KEY|g" \
	-e "s|S3_HOST|$S3_HOST|g" -e "s|S3_PORT|$S3_PORT|g" \
	-e "s|S3_SECRET_KEY|$S3_SECRET_KEY|g" \
	/s3cfg >/tmp/.s3cfg
    while true
    do
	if s4cmd -c /tmp/.s3cfg ls; then
	    echo " s3 is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach s3" >&2
	    exit 1
	fi
	sleep 5
	echo s3 is ... KO
	cpt=`expr $cpt + 1`
    done
    rm -f /tmp/.s3cfg
fi
if test "$OPENLDAP_HOST"; then
    echo Waiting for LDAP backend ...
    cpt=0
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP is ... KO
	cpt=`expr $cpt + 1`
    done
    if test "$OPENLDAP_PROTO" = ldaps; then
	if ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    LDAP_TLS_NO_VERIFY=0
	fi
    fi
fi
if test "$DO_FULLTEXTSEARCH" = true -a -d /var/www/html/custom_apps/fulltextsearch; then
    echo Check for ElasticSearch backend ...
    if ! curl -k --connect-timeout 5 \
	    $ELASTICSEARCH_PROTO://$ELASTICSEARCH_HOST:$ELASTICSEARCH_PORT \
	    2>/dev/null \
	    | grep 'You Know, for Search' >/dev/null; then
	echo "Could not reach ElasticSearch!"
	DO_FULLTEXTSEARCH=false
    else
	echo OK
    fi
fi
if test "$LOOL_ENDPOINT"; then
    echo Check for LOOL backend ...
    if ! curl -k $LOOL_PROTO://$LOOL_ENDPOINT \
	    --connect-timeout 5 2>/dev/null \
	    | grep OK >/dev/null; then
	echo "Could not reach LOOL!"
    else
	echo OK
    fi
fi
if test "$DO_GLOBALSITESELECTOR" = true -a -d /var/www/html/custom_app/globalsiteselector -a "$NEXTCLOUD_LOOKUP_HOST"; then
    echo Check for NextCloud Lookup server ...
    cpt=0
    while true
    do
	if curl -k "$PUBLIC_PROTO://$NEXTCLOUD_LOOKUP_HOST/index.html" \
		--connect-timeout 5 2>/dev/null \
		| grep OK >/dev/null; then
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach NextCloud Lookup Server >&2
	    exit 1
	fi
	cpt=`expr $cpt + 1`
	sleep 5
    done
    echo OK
fi

version_greater()
{
    test "$(printf '%s\n' "$@" | sort -t '.' -n -k1,1 -k2,2 -k3,3 -k4,4 | head -n 1)" != "$1"
}

add_to_config()
{
    local key value

    key=$1
    shift
    value=$@
    if ! echo "$value" | grep -E "['\"]" >/dev/null; then
	value="'$value'"
    fi
    if ! grep "^[ ]*'$key'" /var/www/html/config/config.php >/dev/null; then
	cat <<EOF >>/var/www/html/config/config.php
\$CONFIG['$key'] = $value;
EOF
	echo "INFO: added nextcloud configuration - $key=$value"
    else
	sed -i "s|^[ ]*'$key'.*|  '$key' => $value,|" /var/www/html/config/config.php
	echo "INFO: refreshed nextcloud configuration - $key=$value"
    fi
}

directory_empty()
{
    test -z "$(ls -A "$1/")"
}

run_as()
{
    if test "`id -u`" = 0; then
	su - www-data -s /bin/sh -c "$1"
    else
	sh -c "$1"
    fi
}

fix_integrity_check()
{
    run_as 'php ./occ integrity:check-core' >/tmp/integrity-check.tmp
    if ! grep -E 'INVALID_HASH|EXTRA_FILE' /tmp/integrity-check.tmp; then
	rm -f /tmp/integrity-check.tmp
	return 0
    fi >/dev/null
    cat ./core/signature.json >./config/old-core-signature.json
    cat /tmp/integrity-check.tmp | awk '
BEG {
    l=""; r=""; f="";
} {
    if (NF < 3 && $2 != "expected:") {
	f = substr($2, 1, length($2) - 1);
	l = "";
	r = "";
    } else {
	if ($2 == "expected:") {
	    l = $3;
	} else {
	    if ($2 == "current:") {
		r = $3;
		print "file=" f " has=" r " should=" l;
	    }
	}
    }
}' | while read dome
	do
	    eval "$dome"
	    if test "$DEBUG"; then
		echo "DEBUG: processing $dome"
	    fi
	    if test -z "$file" -o "$file" = expected:; then
		echo "FIXME: could not parse $dome"
		continue
	    elif test -z "$has"; then
		matchfile=`echo "$file" | sed 's|/|\\/|g'`
		sed -i "/.*\"$matchfile\": /d" ./core/signature.json
		echo "NOTICE: removed $file from nextcloud signatures"
	    elif test -z "$should"; then
		matchfile=`echo "$file" | sed 's|/|\\/|g'`
		head -2 ./core/signature.json >/tmp/new-signature.json
		echo "	\"$matchfile\": \"$has\"," >>/tmp/new-signature.json
		cnt=`awk 'END{print NR}' ./core/signature.json`
		tail -`expr $cnt - 1` ./core/signature.json >>/tmp/new-signature.json
		cat /tmp/new-signature.json >./core/signature.json
		echo "NOTICE: added $file to nextcloud signatures"
	    else
		sed -i "s|$should|$has|" ./core/signature.json
		echo "NOTICE: patched hash for $file in nextcloud signatures"
	    fi
	done
    rm -f /tmp/integrity-check.tmp /tmp/new-signature.json
}

install_configmap_if_present()
{
    if test -s /var/k8s/config.php -a -d /var/www/html/config; then
	cat /var/k8s/config.php >/var/www/html/config/config.php
	chown www-data:root /var/www/html/config/config.php
	touch "$NEXTCLOUD_DATA_DIR/.ocdata" 2>/dev/null
    fi
}

patch_htaccess()
{
    if ! grep /server-status /var/www/html/.htaccess >/dev/null; then
	if ! awk '{if (match($3, /well-known/)) { print "  RewriteCond %{REQUEST_URI} !=/server-status"; print "  RewriteCond %{REQUEST_URI} !/apps/globalsiteselector/autologin.*"; } print $0;}' \
		/var/www/html/.htaccess >/var/www/html/.htaccess.patched; then
	    echo failed patching htaccess >&2
	elif ! mv /var/www/html/.htaccess.patched /var/www/html/.htaccess; then
	    echo failed installing patched htaccess >&2
	    if ! cp /usr/src/nextcloud/.htaccess /var/www/html/.htaccess; then
		echo and failed restoring original >&2
	    fi
	fi
    fi
}

update_k8s_conf()
{
    SECRET_DIR=/run/secrets/kubernetes.io/serviceaccount
    if test -s "$SECRET_DIR/token" -a "$NEXTCLOUD_K8S_NAMESPACE" \
	    -a -x /usr/bin/kubectl; then
	K8S_TOKEN=`cat $SECRET_DIR/token`
	if ! ( cd /var/www/html/config \
		&& kubectl "--token=$K8S_TOKEN" --insecure-skip-tls-verify \
		    create configmap "$NEXTCLOUD_CONFIGMAP_NAME" \
		    --from-file=./config.php --dry-run -o yaml \
		    | kubectl "--token=$K8S_TOKEN" --insecure-skip-tls-verify \
		    apply -n "$NEXTCLOUD_K8S_NAMESPACE" -f-
		); then
	    echo "WARNING: Uploading NextCloud conf to K8s returned $?!"
	fi
	unset K8S_TOKEN
    fi
    unset SECRET_DIR
}

installed_version=0.0.0.0
install_configmap_if_present
if test -f /var/www/html/version.php; then
    installed_version="$($PHPCMD -r 'require "/var/www/html/version.php"; echo implode(".", $OC_Version);' 2>/dev/null)"
elif test -s /var/k8s/config.php; then
    installed_version=$(awk "/'version'/{print \$3}" /var/k8s/config.php | sed "s|[,']*||g" 2>/dev/null)
fi
image_version="$($PHPCMD -r 'require "/usr/src/nextcloud/version.php"; echo implode(".", $OC_Version);' 2>/dev/null)"
if version_greater "$installed_version" "$image_version"; then
    cat <<EOF
Can't start Nextcloud because the version of the data ($installed_version)
is higher than the docker image version ($image_version) and downgrading is
not supported. Are you sure you have pulled the newest image version?"
EOF
    exit 1
fi

RESET_AUTH=false
if test "`id -u`" = 0; then
    rsync_options="-rlDog --chown www-data:root"
else
    rsync_options=-rlD
fi
if ! test -d /var/www/html/apps; then
    rsync $rsync_options --delete --exclude /config/ --exclude /data/ \
	--exclude /custom_apps/ --exclude /themes/ \
	/usr/src/nextcloud/ /var/www/html/
    for dir in config data custom_apps themes
    do
	if [ ! -d "/var/www/html/$dir" ] || \
		directory_empty "/var/www/html/$dir"; then
	    rsync $rsync_options --include "/$dir/" --exclude '/*' \
		/usr/src/nextcloud/ /var/www/html/
	fi
    done
    WAS_INIT=true
elif test "$NEXTCLOUD_UPDATE_PLUGINS"; then
    rsync $rsync_options /usr/src/nextcloud/custom_apps/ \
	/var/www/html/custom_apps/
fi
install_configmap_if_present
if version_greater "$image_version" "$installed_version"; then
    if test "$installed_version" != 0.0.0.0; then
	run_as "$PHPCMD /var/www/html/occ app:list" \
	    | sed -n "/Enabled:/,/Disabled:/p" >/tmp/list_before
	rsync $rsync_options --delete --exclude /config/ --exclude /data/ \
	    /usr/src/nextcloud/ /var/www/html/
	run_as "$PHPCMD /var/www/html/occ upgrade"
	run_as "$PHPCMD /var/www/html/occ app:list" | \
	    sed -n "/Enabled:/,/Disabled:/p" >/tmp/list_after
	echo "The following apps have beed disabled:"
	diff /tmp/list_before /tmp/list_after | awk -F- '/</{print $2}' \
	    | cut -d: -f1
	rm -f /tmp/list_before /tmp/list_after
	echo "Generating new indices:"
	run_as "$PHPCMD /var/www/html/occ db:add-missing-indices"
	RESET_AUTH=true
    fi
    update_k8s_conf
fi
if ! test -f /var/www/html/apps/files/cron.php; then
    ln -sf /var/www/html/cron.php /var/www/html/apps/files/
fi
rm -f /var/www/html/config/autoconfig.php \
    $NEXTCLOUD_DATA_DIR/nextcloud.log
ln -sf /dev/stdout $NEXTCLOUD_DATA_DIR/nextcloud.log

if ! test -s /var/www/html/config/config.php; then
    if test "$OPENLDAP_HOST"; then
	OPENLDAP_ENCODE="`/bin/echo -n "$OPENLDAP_BIND_PW" | base64`"
    fi
    if test "$OPENLDAP_PROTO" = ldaps; then
	HOSTSTR=ldaps://$OPENLDAP_HOST
    else
	HOSTSTR=$OPENLDAP_HOST
    fi
    if test "$SQLITE_DATABASE"; then
	run_as "$PHPCMD /var/www/html/occ maintenance:install \
		--data-dir='$NEXTCLOUD_DATA_DIR' --database=sqlite \
		--admin-user='$NEXTCLOUD_ADMIN_USER' \
		--admin-pass='$NEXTCLOUD_ADMIN_PASSWORD'"
	if test "$OPENLDAP_HOST"; then
	    sed -e "s LDAP_HOST $HOSTSTR g" \
		-e "s LDAP_BASE $OPENLDAP_BASE g" \
		-e "s|LDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|g" \
		-e "s LDAP_ENCODE $OPENLDAP_ENCODE g" \
		-e "s|LDAP_GROUP|$OPENLDAP_GROUP_FILTER|g" \
		-e "s LDAP_PORT $OPENLDAP_PORT g" \
		-e "s LDAP_NO_VERIFY $LDAP_TLS_NO_VERIFY g" \
		-e "s LDAP_USEROC $OPENLDAP_USERS_OBJECTCLASS g" \
		-e "s SAML_APP_VERS $SAML_VERSION g" \
		/ldap-config.sql | sqlite3 $NEXTCLOUD_DATA_DIR/owncloud.db
	fi
	chown www-data:www-data $NEXTCLOUD_DATA_DIR/owncloud.db
    elif test "$POSTGRES_PASSWORD"; then
	run_as "$PHPCMD /var/www/html/occ maintenance:install \
		--data-dir='$NEXTCLOUD_DATA_DIR' --database=pgsql \
		--database-name='$POSTGRES_DB' \
		--database-user='$POSTGRES_USER' \
		--database-pass='$POSTGRES_PASSWORD' \
		--database-host='$POSTGRES_HOST' \
		--database-port='$POSTGRES_PORT' \
		--admin-user='$NEXTCLOUD_ADMIN_USER' \
		--admin-pass='$NEXTCLOUD_ADMIN_PASSWORD'"
	if test "$OPENLDAP_HOST"; then
	    sed -e "s LDAP_HOST $HOSTSTR g" \
		-e "s LDAP_BASE $OPENLDAP_BASE g" \
		-e "s|LDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|g" \
		-e "s LDAP_ENCODE $OPENLDAP_ENCODE g" \
		-e "s|LDAP_GROUP|$OPENLDAP_GROUP_FILTER|g" \
		-e "s LDAP_NO_VERIFY $LDAP_TLS_NO_VERIFY g" \
		-e "s LDAP_PORT $OPENLDAP_PORT g" \
		-e "s LDAP_USEROC $OPENLDAP_USERS_OBJECTCLASS g" \
		-e "s SAML_APP_VERS $SAML_VERSION g" \
		/ldap-config.sql | PGPASSWORD="$POSTGRES_PASSWORD" psql \
		    -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		    -p "$POSTGRES_PORT" "$POSTGRES_DB"
	fi
    elif test "$MYSQL_PASSWORD"; then
	run_as "$PHPCMD /var/www/html/occ maintenance:install \
		--data-dir='$NEXTCLOUD_DATA_DIR' --database=mysql \
		--database-name='$MYSQL_DATABASE' \
		--database-user='$MYSQL_USER' \
		--database-pass='$MYSQL_PASSWORD' \
		--database-host='$MYSQL_HOST' \
		--database-port='$MYSQL_PORT' \
		--admin-user='$NEXTCLOUD_ADMIN_USER' \
		--admin-pass='$NEXTCLOUD_ADMIN_PASSWORD'"
	if test "$OPENLDAP_HOST"; then
	    sed -e "s LDAP_HOST $HOSTSTR g" \
		-e "s LDAP_BASE $OPENLDAP_BASE g" \
		-e "s|LDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|g" \
		-e "s LDAP_ENCODE $OPENLDAP_ENCODE g" \
		-e "s|LDAP_GROUP|$OPENLDAP_GROUP_FILTER|g" \
		-e "s LDAP_NO_VERIFY $LDAP_TLS_NO_VERIFY g" \
		-e "s LDAP_PORT $OPENLDAP_PORT g" \
		-e "s LDAP_USEROC $OPENLDAP_USERS_OBJECTCLASS g" \
		-e "s SAML_APP_VERS $SAML_VERSION g" \
		-e 's "oc_appconfig" \`oc_appconfig\` g' \
		/ldap-config.sql | mysql -u "$MYSQL_USER" \
		    --password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		    -P "$MYSQL_PORT" "$MYSQL_DATABASE"
	fi
    fi
    unset OPENLDAP_ENCODE
    rm -f /var/www/html/config/autoconfig.php
    touch "$NEXTCLOUD_DATA_DIR/.ocdata" 2>/dev/null
    run_as "$PHPCMD /var/www/html/occ config:system:set trusted_domains 0 --value=$APACHE_DOMAIN"
    run_as "$PHPCMD /var/www/html/occ config:system:set overwrite.cli.url --value=$APACHE_DOMAIN"
    if test "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" -a "$S3_HOST"; then
	if ! run_as "$PHPCMD /var/www/html/occ app:enable files_external"; then
	   echo WARNING: failed configuring NextCloud external storage >&2
	else
	    echo NOTICE: done configuring NextCloud external storage
	    if test "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" -a "$S3_HOST"; then
		sed -e "s|S3_ACCESS_KEY|$S3_ACCESS_KEY|g" \
		    -e "s|S3_BUCKET|$S3_BUCKET|g" \
		    -e "s|S3_HOST|$S3_HOST|g" \
		    -e "s|S3_PORT|$S3_PORT|g" \
		    -e "s|S3_REGION|$S3_REGION|g" \
		    -e "s|S3_SECRET_KEY|$S3_SECRET_KEY|g" \
		    /s3-config.patch >/var/www/html/config/s3-config.patch
		(
		    cd /var/www/html/config
		    cp -p config.php config0.php
		    if ! patch -p0 <s3-config.patch; then
			echo WARNING: Failed configuring s3 connector >&2
		    else
			echo NOTICE: Successfully initialized s3 connector
			rm -f s3-config.patch
		    fi
		    cp -p config.php config1.php
		)
	    fi
	fi
    elif ! run_as "$PHPCMD /var/www/html/occ app:disable files_external"; then
       echo WARNING: failed disabling NextCloud external storage >&2
    fi
    chown www-data:root /var/www/html/config/config.php \
	"$NEXTCLOUD_DATA_DIR/.ocdata"
    chmod 0664 /var/www/html/config/config.php
    update_k8s_conf
fi
if test "$OTHER_TRUSTED_DOMAINS"; then
    COUNTER=1
    for DOMAIN in $(echo $OTHER_TRUSTED_DOMAINS | tr "," "\n")
    do
	run_as "$PHPCMD /var/www/html/occ config:system:set trusted_domains $COUNTER --value=$DOMAIN"
	COUNTER=`expr $COUNTER + 1`
    done
    unset COUNTER
fi
if test "$OPENLDAP_HOST"; then
    run_as "$PHPCMD /var/www/html/occ app:enable user_ldap"
    run_as "$PHPCMD /var/www/html/occ app:enable password_policy"
    if test -s /saml/lemon.crt; then
	cat /saml/lemon.crt >/var/www/html/config/saml-cert.pem
    elif ! LDAPTLS_REQCERT=never myldapsearch \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    -b "$OPENLDAP_CONF_DN_PREFIX,$OPENLDAP_BASE" \
	    -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/ \
	    | grep -A32 '^description:: {samlServicePublicKeySig}' \
	    | sed 's|description:: {samlServicePublicKeySig}||' \
	    | awk 'BEG{ took=0; }{ if (took != 0) { if ($0 == "" || $0 ~ /description/) { exit 0; }} print $0;took=1; }' \
	    >/var/www/html/config/saml-cert.pem 2>/dev/null; then
	mv /var/www/html/config/saml-cert.pem /var/www/html/config/saml-cert.pem.failed
	cat <<EOF >&2
WARNING: Failed querying $OPENLDAP_PROTO://$OPENLDAP_HOST for SAML signing certificate
WARNING: may not be able to authenticate SAML users
EOF
    fi
    if test -s /var/www/html/config/saml-cert.pem -a "$SAML_LOCKOUT"; then
	if test "$SQLITE_DATABASE"; then
	    (
		sed -e "s LLNG_SSO_DOMAIN $LLNG_SSO_DOMAIN g" \
		    -e "s LLNG_PROTO $LLNG_PROTO g" \
		    -e "s SAML_ADMIN_USERNAME $SAML_ADMIN_USER g" \
		    -e "s| ON CONFLICT DO NOTHING||g" \
		    -e "s SAML_APP_VERS $SAML_VERSION g" /llng-config.sql
		grep -v 'BEGIN CERTIFICATE' \
		    /var/www/html/config/saml-cert.pem
		echo "');"
	    ) | sqlite3 $NEXTCLOUD_DATA_DIR/owncloud.db
	elif test "$POSTGRES_PASSWORD"; then
	    (
		sed -e "s LLNG_SSO_DOMAIN $LLNG_SSO_DOMAIN g" \
		    -e "s LLNG_PROTO $LLNG_PROTO g" \
		    -e "s SAML_ADMIN_USERNAME $SAML_ADMIN_USER g" \
		    -e "s SAML_APP_VERS $SAML_VERSION g" /llng-config.sql
		grep -v 'BEGIN CERTIFICATE' \
		    /var/www/html/config/saml-cert.pem
		echo "');"
	    ) | PGPASSWORD="$POSTGRES_PASSWORD" psql -U "$POSTGRES_USER" \
		-h "$POSTGRES_HOST" -p "$POSTGRES_PORT" "$POSTGRES_DB"
	elif test "$MYSQL_PASSWORD"; then
	    (
		sed -e "s LLNG_SSO_DOMAIN $LLNG_SSO_DOMAIN g" \
		    -e "s LLNG_PROTO $LLNG_PROTO g" \
		    -e "s SAML_ADMIN_USERNAME $SAML_ADMIN_USER g" \
		    -e "s SAML_APP_VERS $SAML_VERSION g" \
		    -e "s| CONFLICT DO UPDATE| DUPLICATE KEY UPDATE gid=gid|g" \
		    -e 's "oc_group_user" \`oc_group_user\` g' \
		    -e 's "oc_appconfig" \`oc_appconfig\` g' /llng-config.sql
		grep -v 'BEGIN CERTIFICATE' \
		    /var/www/html/config/saml-cert.pem
		echo "');"
	    ) | mysql -u "$MYSQL_USER" --password="$MYSQL_PASSWORD" \
		-h "$MYSQL_HOST" -P "$MYSQL_PORT" "$MYSQL_DATABASE"
	fi
	run_as '$PHPCMD /var/www/html/occ app:enable user_saml'
    fi
fi

if test "$SAML_LOCKOUT" -a "$LLNG_PROTO" = https; then
    add_to_config overwriteprotocol https
fi
if test "$APPS_CACHE"; then
    if echo "$APPS_CACHE" | grep https://apps.nextcloud.com >/dev/null; then
	sed -i -e "/appstoreenabled/d" \
	       -e "/appstoreurl/d" \
	   /var/www/html/config/config.php
    else
	if echo "$APPS_CACHE" | grep cluster.local >/dev/null; then
	    APPS_CACHE=`echo "$APPS_CACHE" | sed 's|.cluster.local||'`
	fi
	add_to_config appstoreenabled true
	add_to_config appstoreurl "$APPS_CACHE/api/v1"
    fi
fi
if test "$SMTP_HOST"; then
    add_to_config mail_from_address $MAIL_FROM_USER
    add_to_config mail_smtpmode $MAIL_SMTP_MODE
    add_to_config mail_sendmailmode $MAIL_SENDMAIL_MODE
    add_to_config mail_domain $OPENLDAP_DOMAIN
    add_to_config mail_smtphost $SMTP_HOST
    add_to_config mail_smtpport $SMTP_PORT
fi
run_as "$PHPCMD /var/www/html/occ app:enable recommendations"
run_as "$PHPCMD /var/www/html/occ app:disable updatenotification"

if test "$LOOL_ENDPOINT" -a ! -d /var/www/html/custom_apps/richdocuments; then
    echo "WARNING: image does not ship with lool support"
    echo "INFO: ignoring lool integration configuration"
elif test "$LOOL_ENDPOINT"; then
    run_as "$PHPCMD /var/www/html/occ app:enable richdocuments"
    run_as "$PHPCMD /var/www/html/occ config:app:set richdocuments disable_certificate_verification --value yes"
    run_as "$PHPCMD /var/www/html/occ config:app:set richdocuments external_apps --value \"\""
    run_as "$PHPCMD /var/www/html/occ config:app:set richdocuments public_wopi_url --value $LOOL_PROTO://$LOOL_ENDPOINT"
    run_as "$PHPCMD /var/www/html/occ config:app:set richdocuments types --value filesystem,dav,prevent_group_restriction"
    run_as "$PHPCMD /var/www/html/occ config:app:set richdocuments wopi_url --value $LOOL_PROTO://$LOOL_ENDPOINT"
    run_as "$PHPCMD /var/www/html/occ config:app:set richdocuments enabled --value yes"
    for f in /var/www/html/3rdparty/sabre/dav/lib/DAV/Browser/Plugin.php \
	/var/www/html/remote.php
    do
	if ! test -s $f; then
	    echo WARNING: $f not found
	    continue
	fi
	sed -i \
	    "s|default-src 'none';|default-src 'none'; frame-src $LOOL_PROTO://$LOOL_ENDPOINT; frame-ancestors $LOOL_PROTO://$LOOL_ENDPOINT; form-action 'self' $LOOL_PROTO://$LOOL_ENDPOINT $LOOL_PROTO://$APACHE_DOMAIN|" \
	    $f
    done
elif test -d /var/www/html/custom_apps/richdocuments; then
    run_as "$PHPCMD /var/www/html/occ app:disable richdocuments"
fi

if test "$DO_DRAW" = true -a ! -d /var/www/html/custom_apps/drawio; then
    echo "WARNING: image does not ship with draw plugin"
    echo "INFO: ignoring draw integration configuration"
elif test "$DO_DRAW" = true -a -d /var/www/html/custom_app/drawio; then
    run_as "$PHPCMD /var/www/html/occ app:enable drawio"
    if test "$DRAW_DOMAIN" -a "$DRAW_DOMAIN" != embed.diagrams.net -a -s \
	    /var/www/html/custom_apps/drawio/lib/appconfig.php; then
	sed -i "s|https://embed.diagrams.net|$PUBLIC_PROTO://$DRAW_DOMAIN|" \
	    /var/www/html/custom_apps/drawio/lib/appconfig.php
    fi
elif test -d /var/www/html/custom_app/drawio; then
    run_as "$PHPCMD /var/www/html/occ app:disable drawio"
fi

if test "$DO_FULLTEXTSEARCH" = true -a ! -d /var/www/html/custom_apps/fulltextsearch; then
    echo "WARNING: image does not ship with fulltextsearch plugin"
    echo "INFO: ignoring fulltextsearch plugin configuration"
elif test "$DO_FULLTEXTSEARCH" = true -a -d /var/www/html/custom_app/fulltextsearch; then
    if test "$SQLITE_DATABASE" -a "$NEXTCLOUD_DATA_LOCATION"; then
	sed -e "s FLFTEXT_VERS $FLFULLTEXT_VERSION g" \
	    -e "s ESFTEXT_VERS $ESFULLTEXT_VERSION g" \
	    -e "s FULLTEXT_VERS $FULLTEXT_VERSION g" \
	    -e "s ES_INDEX $ELASTICSEARCH_INDEX g" \
	    -e "s ES_URL $ELASTICSEARCH_PROTO://$ELASTICSEARCH_HOST:$ELASTICSEARCH_PORT g" \
	    /fulltext.sql | sqlite3 $NEXTCLOUD_DATA_DIR/owncloud.db
    elif test "$POSTGRES_PASSWORD" -a "$NEXTCLOUD_DATA_LOCATION"; then
	sed -e "s FLFTEXT_VERS $FLFULLTEXT_VERSION g" \
	    -e "s ESFTEXT_VERS $ESFULLTEXT_VERSION g" \
	    -e "s FULLTEXT_VERS $FULLTEXT_VERSION g" \
	    -e "s ES_INDEX $ELASTICSEARCH_INDEX g" \
	    -e "s ES_URL $ELASTICSEARCH_PROTO://$ELASTICSEARCH_HOST:$ELASTICSEARCH_PORT g" \
	    /fulltext.sql | PGPASSWORD="$POSTGRES_PASSWORD" psql \
		-U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB"
    elif test "$MYSQL_PASSWORD" -a "$NEXTCLOUD_DATA_LOCATION"; then
	sed -e "s FLFTEXT_VERS $FLFULLTEXT_VERSION g" \
	    -e "s ESFTEXT_VERS $ESFULLTEXT_VERSION g" \
	    -e "s FULLTEXT_VERS $FULLTEXT_VERSION g" \
	    -e "s ES_INDEX $ELASTICSEARCH_INDEX g" \
	    -e "s ES_URL $ELASTICSEARCH_PROTO://$ELASTICSEARCH_HOST:$ELASTICSEARCH_PORT g" \
	    /fulltext.sql | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE"
    fi
    run_as "$PHPCMD /var/www/html/occ app:enable fulltextsearch"
    run_as "$PHPCMD /var/www/html/occ app:enable files_fulltextsearch"
    run_as "$PHPCMD /var/www/html/occ app:enable fulltextsearch_elasticsearch"
    if ! test -s /var/www/html/.fulltext-index-built; then
	(
	    if ! run_as "$PHPCMD /var/www/html/occ fulltextsearch:test"; then
		echo WARNING: fulltextsearch test failed
	    elif ! run_as "$PHPCMD /var/www/html/occ fulltextsearch:index" 2>&1 \
		    | tee -a /var/www/html/.fulltext-index-built; then
		echo WARNING: fulltextsearch initial index build failed
		mv /var/www/html/.fulltext-index-built \
		     /var/www/html/.fulltext-index-build-failed
	    fi
	) &
    fi
elif test -d /var/www/html/custom_app/fulltextsearch; then
    run_as "$PHPCMD /var/www/html/occ app:disable fulltextsearch"
    run_as "$PHPCMD /var/www/html/occ app:disable files_fulltextsearch"
    run_as "$PHPCMD /var/www/html/occ app:disable fulltextsearch_elasticsearch"
fi

if test "$DO_GLOBALSITESELECTOR" = true -a ! -d /var/www/html/custom_apps/globalsiteselector; then
    echo "WARNING: image does not ship with globalsiteselector plugin"
    echo "INFO: ignoring globalsiteselector plugin configuration"
elif test "$DO_GLOBALSITESELECTOR" = true -a -d /var/www/html/custom_app/globalsiteselector -a "$NEXTCLOUD_LOOKUP_HOST"; then
    add_to_config lookup_server $PUBLIC_PROTO://$NEXTCLOUD_LOOKUP_HOST
    add_to_config gss.jwt.key $LOOKUP_JWT_KEY
    add_to_config gss.master.csp-allow "['*.$OPENLDAP_DOMAIN','$APACHE_DOMAIN','$NEXTCLOUD_LOOKUP_HOST']"
    if test "$NEXTCLOUD_GSS_MASTER" = $APACHE_DOMAIN; then
	add_to_config gs.enabled true
	add_to_config gss.mode master
	add_to_config gss.master.admin "['$NEXTCLOUD_ADMIN_USER']"
	if test -s /var/www/html/config/saml-cert.pem -a "$SAML_LOCKOUT"; then
	    add_to_config gss.user.discovery.module '\OCA\GlobalSiteSelector\UserDiscoveryModules\UserDiscoverySAML'
	else
	    add_to_config gss.user.discovery.module '\OCA\GlobalSiteSelector\UserDiscoveryModules\ManualUserMapping'
	    add_to_config gss.discovery.manual.mapping.parameter idp-parameter
	    add_to_config gss.discovery.manual.mapping.file /var/www/html/config/user-mapping.json
	    if ! test -s /var/www/html/config/user-mapping.json; then
		if test -s "$NEXTCLOUD_GSS_USERMAPPING"; then
		    cat "$NEXTCLOUD_GSS_USERMAPPING" >/var/www/html/config/user-mapping.json
		else
		    echo WARNING: missing input "$NEXTCLOUD_GSS_USERMAPPING" configuration
		fi
	    fi
	fi
    else
	add_to_config gss.mode slave
	add_to_config gss.master.url $NEXTCLOUD_GSS_MASTER
    fi
    run_as "$PHPCMD /var/www/html/occ app:enable globalsiteselector"
    run_as "$PHPCMD /var/www/html/occ app:enable federation"
elif test -d /var/www/html/custom_app/globalsiteselector; then
    run_as "$PHPCMD /var/www/html/occ app:disable globalsiteselector"
    run_as "$PHPCMD /var/www/html/occ app:disable federation"
fi

if test "$DO_GROUPFOLDERS" = true -a ! -d /var/www/html/custom_apps/groupfolders; then
    echo "WARNING: image does not ship with groupfolders plugin"
    echo "INFO: ignoring groupfolders plugin configuration"
elif test "$DO_GROUPFOLDERS" = true -a -d /var/www/html/custom_app/groupfolders; then
    run_as "$PHPCMD /var/www/html/occ app:enable groupfolders"
elif test -d /var/www/html/custom_app/groupfolders; then
    run_as "$PHPCMD /var/www/html/occ app:disable groupfolders"
fi

if test "$DO_IMPERSONATE" = true -a ! -d /var/www/html/custom_apps/impersonate; then
    echo "WARNING: image does not ship with impersonate plugin"
    echo "INFO: ignoring impersonate plugin configuration"
elif test "$DO_IMPERSONATE" = true -a -d /var/www/html/custom_app/impersonate; then
    run_as "$PHPCMD /var/www/html/occ app:enable impersonate"
elif test -d /var/www/html/custom_app/impersonate; then
    run_as "$PHPCMD /var/www/html/occ app:disable impersonate"
fi

if test "$DO_MATOMO" = true -a ! -d /var/www/html/custom_apps/piwik; then
    echo "WARNING: image does not ship with matomo plugin"
    echo "INFO: ignoring matomo plugin configuration"
elif test "$DO_MATOMO" = true -a -d /var/www/html/custom_app/piwik; then
    if test "$SQLITE_DATABASE" -a "$MATOMO_URL"; then
	cat <<EOF | sqlite3 $NEXTCLOUD_DATA_DIR/owncloud.db
DELETE FROM "oc_appconfig" WHERE appid = 'piwik';
INSERT INTO "oc_appconfig" VALUES ('piwik','siteId','$MATOMO_SITE_ID');
INSERT INTO "oc_appconfig" VALUES ('piwik','trackDir','$MATOMO_TRACK_DIR');
INSERT INTO "oc_appconfig" VALUES ('piwik','trackUser','$MATOMO_TRACK_USER');
INSERT INTO "oc_appconfig" VALUES ('piwik','installed_version','$MATOMO_VERSION');
INSERT INTO "oc_appconfig" VALUES ('piwik','enabled','yes');
INSERT INTO "oc_appconfig" VALUES ('piwik','types','');
INSERT INTO "oc_appconfig" VALUES ('piwik','url','$MATOMO_URL');
EOF
    elif test "$POSTGRES_PASSWORD" -a "$MATOMO_URL"; then
	cat <<EOF | PGPASSWORD="$POSTGRES_PASSWORD" psql \
		-U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB"
DELETE FROM "oc_appconfig" WHERE appid = 'piwik';
INSERT INTO "oc_appconfig" VALUES ('piwik','siteId','$MATOMO_SITE_ID');
INSERT INTO "oc_appconfig" VALUES ('piwik','trackDir','$MATOMO_TRACK_DIR');
INSERT INTO "oc_appconfig" VALUES ('piwik','trackUser','$MATOMO_TRACK_USER');
INSERT INTO "oc_appconfig" VALUES ('piwik','installed_version','$MATOMO_VERSION');
INSERT INTO "oc_appconfig" VALUES ('piwik','enabled','yes');
INSERT INTO "oc_appconfig" VALUES ('piwik','types','');
INSERT INTO "oc_appconfig" VALUES ('piwik','url','$MATOMO_URL');
EOF
    elif test "$MYSQL_PASSWORD" -a "$MATOMO_URL"; then
	cat <<EOF | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE"
DELETE FROM "oc_appconfig" WHERE appid = 'piwik';
INSERT INTO "oc_appconfig" VALUES ('piwik','siteId','$MATOMO_SITE_ID');
INSERT INTO "oc_appconfig" VALUES ('piwik','trackDir','$MATOMO_TRACK_DIR');
INSERT INTO "oc_appconfig" VALUES ('piwik','trackUser','$MATOMO_TRACK_USER');
INSERT INTO "oc_appconfig" VALUES ('piwik','installed_version','$MATOMO_VERSION');
INSERT INTO "oc_appconfig" VALUES ('piwik','enabled','yes');
INSERT INTO "oc_appconfig" VALUES ('piwik','types','');
INSERT INTO "oc_appconfig" VALUES ('piwik','url','$MATOMO_URL');
EOF
    fi
    run_as "$PHPCMD /var/www/html/occ app:enable piwik"
elif test -d /var/www/html/custom_app/piwik; then
    run_as "$PHPCMD /var/www/html/occ app:disable piwik"
fi

if test "$DO_MDED" = true -a ! -d /var/www/html/custom_apps/files_markdown; then
    echo "WARNING: image does not ship with files_markdown plugin"
    echo "INFO: ignoring files_markdown plugin configuration"
elif test "$DO_MDED" = true -a -d /var/www/html/custom_app/files_markdown; then
    run_as "$PHPCMD /var/www/html/occ app:enable files_markdown"
elif test -d /var/www/html/custom_app/files_markdown; then
    run_as "$PHPCMD /var/www/html/occ app:disable files_markdown"
fi

if test "$DO_PASSWORDS" = true -a ! -d /var/www/html/custom_apps/passwords; then
    echo "WARNING: image does not ship with passwords plugin"
    echo "INFO: ignoring passwords plugin configuration"
elif test "$DO_PASSWORDS" = true -a -d /var/www/html/custom_app/passwords; then
    run_as "$PHPCMD /var/www/html/occ app:enable passwords"
elif test -d /var/www/html/custom_app/passwords; then
    run_as "$PHPCMD /var/www/html/occ app:disable passwords"
fi

if test "$DO_QUOTAWARNING" = true -a ! -d /var/www/html/custom_apps/quota_warning; then
    echo "WARNING: image does not ship with quota_warning plugin"
    echo "INFO: ignoring quota_warning plugin configuration"
elif test "$DO_QUOTAWARNING" = true -a -d /var/www/html/custom_app/quota_warning; then
    run_as "$PHPCMD /var/www/html/occ app:enable quota_warning"
elif test -d /var/www/html/custom_app/quota_warning; then
    run_as "$PHPCMD /var/www/html/occ app:disable quota_warning"
fi

if test "$DO_ADMINAUDIT" = true -a ! -d /var/www/html/apps/admin_audit; then
    echo "WARNING: image does not ship with admin_audit plugin"
    echo "INFO: ignoring admin_audit plugin configuration"
elif test "$DO_ADMINAUDIT" = true -a -d /var/www/html/apps/admin_audit; then
    run_as "$PHPCMD /var/www/html/occ app:enable admin_audit"
elif test -d /var/www/html/apps/admin_audit; then
    run_as "$PHPCMD /var/www/html/occ app:disable admin_audit"
fi

if test "$DO_PRIVACY" = true -a ! -d /var/www/html/apps/privacy; then
    echo "WARNING: image does not ship with privacy plugin"
    echo "INFO: ignoring privacy plugin configuration"
elif test "$DO_PRIVACY" = true -a -d /var/www/html/apps/privacy; then
    run_as "$PHPCMD /var/www/html/occ app:enable privacy"
    if test "$SQLITE_DATABASE" -a "$NEXTCLOUD_DATA_LOCATION"; then
	cat <<EOF | sqlite3 $NEXTCLOUD_DATA_DIR/owncloud.db
DELETE FROM "oc_appconfig" WHERE appid = 'privacy' AND configkey = 'readableLocation';
INSERT INTO "oc_appconfig" VALUES ('privacy','readableLocation','$NEXTCLOUD_DATA_LOCATION');
EOF
    elif test "$POSTGRES_PASSWORD" -a "$NEXTCLOUD_DATA_LOCATION"; then
	cat <<EOF | PGPASSWORD="$POSTGRES_PASSWORD" psql \
		-U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB"
DELETE FROM "oc_appconfig" WHERE appid = 'privacy' AND configkey = 'readableLocation';
INSERT INTO "oc_appconfig" VALUES ('privacy','readableLocation','$NEXTCLOUD_DATA_LOCATION');
EOF
    elif test "$MYSQL_PASSWORD" -a "$NEXTCLOUD_DATA_LOCATION"; then
	cat <<EOF | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE"
DELETE FROM "oc_appconfig" WHERE appid = 'privacy' AND configkey = 'readableLocation';
INSERT INTO "oc_appconfig" VALUES ('privacy','readableLocation','$NEXTCLOUD_DATA_LOCATION');
EOF
    fi
elif test -d /var/www/html/apps/privacy; then
    run_as "$PHPCMD /var/www/html/occ app:disable privacy"
fi

if test "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" -a "$S3_HOST"; then
    if ! run_as "$PHPCMD /var/www/html/occ app:enable files_external"; then
	echo WARNING: failed enabling NextCloud external storage >&2
    fi
elif ! run_as "$PHPCMD /var/www/html/occ app:disable files_external"; then
    echo WARNING: failed disabling NextCloud external storage >&2
fi

if test "$NEXTCLOUD_CRON_INTERVAL" -ge 59 >/dev/null 2>&1; then
    JOBMODE=cron
elif test "$NEXTCLOUD_CRON_INTERVAL" = sidecar; then
    echo NOTICE: disables ajax cron, assuming job sidecar was started
    JOBMODE=cron
else
    JOBMODE=ajax
fi
if test "$SQLITE_DATABASE"; then
    run_as "$PHP_CMD /var/www/html/occ config:app:set core backgroundjobs_mode --value=$JOBMODE"
fi
if test "$REDIS_HOST"; then
    REDIS_DB=${REDIS_DB:-0}
    REDIS_PORT=${REDIS_PORT:-6379}
    if ! grep "'redis'" /var/www/html/config/config.php >/dev/null; then
	if test "$REDIS_PASSWORD"; then
	    cat <<EOF >>/var/www/html/config/config.php
\$CONFIG['filelocking.enabled'] = true;
\$CONFIG['memcache.locking'] = '\\OC\\Memcache\\Redis';
\$CONFIG['redis'] = array(
	'dbindex' => $REDIS_DB,
	'host' => '$REDIS_HOST',
	'password' => '$REDIS_PASSWORD',
	'port' => $REDIS_PORT,
	'timeout' => 0.0,
    );
EOF
	else
	    cat <<EOF >>/var/www/html/config/config.php
\$CONFIG['filelocking.enabled'] = true;
\$CONFIG['memcache.locking'] = '\\OC\\Memcache\\Redis';
\$CONFIG['redis'] = array(
	'dbindex' => $REDIS_DB,
	'host' => '$REDIS_HOST',
	'port' => $REDIS_PORT,
	'timeout' => 0.0,
    );
EOF
	fi
    fi
fi

if $WAS_INIT; then
    for plugin in firstrunwizard support password_policy federation \
	    federatedfilesharing recommendations nextcloud_announcements \
	    updatenotification survey_client;
    do
	run_as "$PHPCMD /var/www/html/occ app:disable $plugin"
    done
    run_as "$PHPCMD /var/www/html/occ upgrade"
    RESET_AUTH=true
fi
if $RESET_AUTH; then
    if test "$OPENLDAP_HOST"; then
	run_as "$PHPCMD /var/www/html/occ app:disable password_policy"
	run_as "$PHPCMD /var/www/html/occ app:enable password_policy"
	run_as "$PHPCMD /var/www/html/occ app:disable user_ldap"
	run_as "$PHPCMD /var/www/html/occ app:enable user_ldap"
    fi
    if test -s /var/www/html/config/saml-cert.pem; then
	run_as "$PHPCMD /var/www/html/occ app:disable user_saml"
	run_as "$PHPCMD /var/www/html/occ app:enable user_saml"
    fi
fi
if $DO_SOCIALS; then
    run_as "$PHPCMD /var/www/html/occ config:app:set settings show_social_buttons --value=yes"
else
    run_as "$PHPCMD /var/www/html/occ config:app:set settings show_social_buttons --value=no"
fi
if $DO_REASONS; then
    run_as "$PHPCMD /var/www/html/occ config:app:set settings show_reasons_use_nextcloud --value=yes"
else
    run_as "$PHPCMD /var/www/html/occ config:app:set settings show_reasons_use_nextcloud --value=no"
fi
add_to_config log_type errorlog
add_to_config loglevel $LOGLEVEL

if test "$NEXTCLOUD_CRON_INTERVAL" -ge 59 >/dev/null 2>&1; then
    echo WARNING: Starting jobs in background
    echo A separate pod with its entrypoint set to job.sh would be better
    /job.sh &
fi

ls /certs/*.crt 2>/dev/null | while read f
    do
	if ! openssl x509 -text -noout -in $f 2>/dev/null \
		| grep CA:TRUE >/dev/null; then
	    continue
	fi
	lastrow=$(cat $f | tail -2 | head -1)
	if ! grep "$lastrow" \
		/var/www/html/resources/config/ca-bundle.crt >/dev/null; then
	    cat $f >>/var/www/html/resources/config/ca-bundle.crt
	fi
	#obviously not a perfect check....
	#that would do for now
    done

patch_htaccess

#fix_integrity_check
if ! grep integrity.check.disabled /var/www/html/config/config.php >/dev/null; then
    cat <<EOF >>/var/www/html/config/config.php
\$CONFIG['integrity.check.disabled'] = true;
EOF
fi

update_k8s_conf

if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    sed -e "s HTTP_HOST $APACHE_DOMAIN g" \
	-e "s HTTP_PORT $APACHE_HTTP_PORT g" \
	-e "s SSL_CONF /etc/apache2/$SSL_INCLUDE.conf g" \
	/vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf
fi

unset MYSQL_USER MYSQL_PASSWORD POSTGRES_USER POSTGRES_PASSWORD DO_ADMINAUDIT \
    OPENLDAP_BIND_PW OPENLDAP_BASE OPENLDAP_HOST LOOL_PROTO JOBMODE RESET_AUTH \
    OPENLDAP_PORT OPENLDAP_BIND_DN_PREFIX LLNG_SSO_DOMAIN GROUPFOLDERS_VERSION \
    SAML_ADMIN_USER OTHER_TRUSTED_DOMAINS NEXTCLOUD_DATA_DIR LOOL_PORT DO_DRAW \
    OPENLDAP_GROUP_FILTER LOOL_WS_PROTO SAML_LOCKOUT DO_IMPERSONATE LLNG_PROTO \
    NEXTCLOUD_K8S_NAMESPACE MAIL_SENDMAIL_MODE SMTP_HOST SMTP_PORT SSL_INCLUDE \
    DO_GROUPFOLDERS DO_PRIVACY LOOL_VERSION LOOL_ENDPOINT NEXTCLOUD_ADMIN_USER \
    OPENLDAP_DOMAIN has_lool installed_version image_version rsync_options cpt \
    OPENLDAP_USERS_OBJECTCLASS NEXTCLOUD_DATA_LOCATION NEXTCLOUD_CRON_INTERVAL \
    SAML_VERSION NEXTCLOUD_CONFIGMAP_NAME S3_REGION S3_SECRET_KEY S3_PROTO \
    S3_VERSION S3_ACCESS_KEY S3_BUCKET S3_HOST S3_PORT WAS_INIT DO_PASSWORDS \
    LOOL_ENDPOINT LOOL_DO_PROXY HOSTSTR MAIL_FROM_USER MAIL_SMTP_MODE DO_MDED \
    NEXTCLOUD_ADMIN_PASSWORD DO_LOOL DO_QUOTAWARNING NEXTCLOUD_DATA_LOCATION \
    NEXTCLOUD_CRON_INTERVAL OPENLDAP_CONF_DN_PREFIX DO_FULLTEXTSEARCH LOGLEVEL \
    LOOKUP_JWT_KEY NEXTCLOUD_GSS_MASTER PHPCMD NEXTCLOUD_LOOKUP_HOST

. /run-apache.sh
