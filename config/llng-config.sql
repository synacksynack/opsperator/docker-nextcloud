INSERT INTO "oc_group_user" VALUES ('admin', 'SAML_ADMIN_USERNAME') ON CONFLICT DO NOTHING;
DELETE FROM "oc_appconfig" WHERE appid = 'user_saml';
INSERT INTO "oc_appconfig" VALUES ('user_saml','installed_version','SAML_APP_VERS');
INSERT INTO "oc_appconfig" VALUES ('user_saml','enabled','yes');
INSERT INTO "oc_appconfig" VALUES ('user_saml','types','authentication');
INSERT INTO "oc_appconfig" VALUES ('user_saml','type','saml');
INSERT INTO "oc_appconfig" VALUES ('user_saml','general-idp0_display_name','LLNG');
INSERT INTO "oc_appconfig" VALUES ('user_saml','general-require_provisioned_account','1');
INSERT INTO "oc_appconfig" VALUES ('user_saml','general-uid_mapping','uid');
INSERT INTO "oc_appconfig" VALUES ('user_saml','general-use_saml_auth_for_desktop','1');
INSERT INTO "oc_appconfig" VALUES ('user_saml','saml-attribute-mapping-quota_mapping','cloudQuota');
INSERT INTO "oc_appconfig" VALUES ('user_saml','idp-entityId','LLNG_PROTO://LLNG_SSO_DOMAIN/saml/metadata');
INSERT INTO "oc_appconfig" VALUES ('user_saml','idp-singleSignOnService.url','LLNG_PROTO://LLNG_SSO_DOMAIN/saml/singleSignOn');
INSERT INTO "oc_appconfig" VALUES ('user_saml','idp-singleLogoutService.url','LLNG_PROTO://LLNG_SSO_DOMAIN/saml/proxySingleLogout');
INSERT INTO "oc_appconfig" VALUES ('user_saml','idp-x509cert','-----BEGIN CERTIFICATE-----
