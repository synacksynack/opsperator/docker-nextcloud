DELETE FROM "oc_appconfig" WHERE appid = 'files_fulltextsearch';
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','enabled','yes');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','installed_version','FLFTEXT_VERS');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_local','1');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_group_folders','1');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','types','filesystem');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_audio','0');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_encrypted','0');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_external','1');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_federated','0');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_image','0');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_office','1');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_pdf','1');
INSERT INTO "oc_appconfig" VALUES ('files_fulltextsearch','files_size','20');

DELETE FROM "oc_appconfig" WHERE appid = 'fulltextsearch_elasticsearch';
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch_elasticsearch','analyzer_tokenizer','standard');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch_elasticsearch','installed_version','ESFTEXT_VERS');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch_elasticsearch','types','');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch_elasticsearch','enabled','yes');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch_elasticsearch','elastic_index','ES_INDEX');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch_elasticsearch','elastic_host','ES_URL');

DELETE FROM "oc_appconfig" WHERE appid = 'fulltextsearch';
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch','app_navigation','1');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch','enabled','yes');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch','installed_version','FULLTEXT_VERS');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch','search_platform','OCA\FullTextSearch_ElasticSearch\Platform\ElasticSearchPlatform');
INSERT INTO "oc_appconfig" VALUES ('fulltextsearch','types','');
