SKIP_SQUASH?=1
MYIP=10.42.42.127
IMAGE=opsperator/nextcloud
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: buildoc
buildoc:
	SETUP_OC=true SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: run
run:
	@@docker run -e NEXTCLOUD_DOMAIN=nextcloud.demo.local \
	    -e NEXTCLOUD_HTTP_PORT=8090 \
	    -e SQLITE_DATABASE=data.sqlite \
	    -e NEXTCLOUD_ADMIN_PASSWORD=sigipsr \
	    -e NEXTCLOUD_ADMIN_USER=admin \
	    -p 8090:8090 $(IMAGE)

.PHONY: ldapdemo
ldapdemo:
	@@docker run -e NEXTCLOUD_DOMAIN=cloud.demo.local \
	    -e NEXTCLOUD_HTTP_PORT=8090 \
	    -e SQLITE_DATABASE=data.sqlite \
	    -e NEXTCLOUD_ADMIN_PASSWORD=sigipsr \
	    -e LDAP_HOST=$(MYIP) \
	    -e NEXTCLOUD_ADMIN_USER=admin \
	    --add-host=auth.demo.local:$(MYIP) \
	    -p 8090:8090 $(IMAGE)

.PHONY: samldemo
samldemo:
	@@docker run -e NEXTCLOUD_DOMAIN=cloud.demo.local \
	    -e NEXTCLOUD_HTTP_PORT=8090 \
	    -e SQLITE_DATABASE=data.sqlite \
	    -e NEXTCLOUD_ADMIN_PASSWORD=sigipsr \
	    -e LDAP_HOST=$(MYIP) \
	    -e NEXTCLOUD_ADMIN_USER=admin \
	    -e SAML_LOCKOUT=pleasedo \
	    --add-host=auth.demo.local:$(MYIP) \
	    -p 8090:8090 $(IMAGE)

.PHONY: pgdemo
pgdemo:
	@@docker run -e NEXTCLOUD_DOMAIN=cloud.demo.local \
	    -e NEXTCLOUD_HTTP_PORT=8090 \
	    -e POSTGRES_DB=nextcloud \
	    -e POSTGRES_HOST=$(MYIP) \
	    -e POSTGRES_USER=nextcloud \
	    -e POSTGRES_PASSWORD=secret \
	    -e LDAP_HOST=$(MYIP) \
	    -e NEXTCLOUD_ADMIN_PASSWORD=sigipsr \
	    -e NEXTCLOUD_ADMIN_USER=admin \
	    --add-host=auth.demo.local:$(MYIP) \
	    -p 8090:8090 $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "NEXTCLOUD_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "NEXTCLOUD_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocprod
ocprod: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    if test -z "$$RADOSGW_KEYRING_NAME" -o \
		    -z "$$RADOSGW_KEYRING_KEY"; then \
		echo missing rados keyring data >&2; \
		exit 1; \
	    elif test -z "$$CEPH_CLUSTER_ID" -o -z "$$CEPH_CLUSTER_NAME"; then \
		echo missing ceph cluster data >&2; \
		exit 1; \
	    elif test -z "$$CEPH_INITIAL_MEMBERS" -o \
		    -z "$$CEPH_MON_HOSTS"; then \
		echo missing ceph monitors data >&2; \
		exit 1; \
	    else \
		oc process -f deploy/openshift/secret-prod.yaml \
		    -p FRONTNAME=$(FRONTNAME) \
		    -p "RADOSGW_KEYRING_NAME=$$RADOSGW_KEYRING_NAME" \
		    -p "RADOSGW_KEYRING_KEY=$$RADOSGW_KEYRING_KEY" \
		    -p "CEPH_CLUSTER_ID=$$CEPH_CLUSTER_ID" \
		    -p "CEPH_CLUSTER_NAME=$$CEPH_CLUSTER_NAME" \
		    -p "CEPH_INITIAL_MEMBERS=$$CEPH_INITIAL_MEMBERS" \
		    -p "CEPH_MON_HOSTS=$$CEPH_MON_HOSTS" \
		    | oc apply -f-; \
	    fi; \
	fi
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc apply -f-

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
