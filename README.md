# k8s NextCloud

NextCloud image.

Based on https://github.com/nextcloud/docker/blob/master/16.0/apache

Diverts from https://github.com/faust64/docker-php

Depends on postgresql or mysql, sqlite capable, can connect with
LDAP and LLNG, radosgw-ready

Build with:

```
$ make build
```

May ship with OpenShift client:

```
$ make buildoc
```

Test with:

```
$ docker run -e NEXTCLOUD_DOMAIN=nextcloud.demo.local \
    -e SQLITE_DATABASE=data.sqlite \
    -e NEXTCLOUD_ADMIN_PASSWORD=sigipsr \
    -e NEXTCLOUD_ADMIN_USER=admin \
    -e NEXTCLOUD_HTTP_PORT=8080 -p 8080:8080 opsperator/nextcloud
```

Add domains to Nextcloud trusted_domains configuration with:

```
    -e OTHER_TRUSTED_DOMAINS=nextcloud-demo,cloud.demo.local \
```

Enable LDAP configuration with:

```
    -e LDAP_BASE=dc=demo,dc=local \
    -e LDAP_DOMAIN=demo.local \
    -e LDAP_HOST=10.42.42.127 \
    -e LDAP_BIND_DN=cn=nextcloud,ou=services,dc=demo,dc=local \
    -e LDAP_BIND_PW=secret \
```

Enable User-SAML based on Lemon:

```
    -e LLNG_PROTO=http \
    -e LLNG_SSO_DOMAIN=auth.demo.local \
    -e SAML_LOCKOUT=yes \
```

Switch from SQLite to Postgres removing `SQLITE_DATABASE` and passing:

```
    -e POSTGRES_DB=nextcloud \
    -e POSTGRES_HOST=10.42.42.83 \
    -e POSTGRES_USER=nextcloud \
    -e POSTGRES_PASSWORD=secret \
```

Switch from file (`/var/www/html/data`) to s3 storage:

```
    -e S3_ACCESS_KEY=radosgw-access-key
    -e S3_BUCKET=my-s3-bucket
    -e S3_SECRET_KEY=radosgw-secret-key
    -e S3_HOST=radosgw
    -e S3_PORT=8080
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                      | Default                                                     | Inherited From    |
| :----------------------------- | ----------------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`               | NextCloud ServerName                | `cloud.${OPENLDAP_DOMAIN}`                                  | opsperator/apache |
|  `APACHE_HTTP_PORT`            | NextCloud HTTP(s) Port              | `8080`                                                      | opsperator/apache |
|  `APACHE_IGNORE_OPENLDAP`      | Ignore LemonLDAP autoconf           | undef                                                       | opsperator/apache |
|  `APPS_CACHE`                  | NextCloud Applications Cache        | undef                                                       |                   |
|  `DO_ADMINAUDIT`               | NextCloud Admin Audit Toggle        | `true`                                                      |                   |
|  `DO_DRAW`                     | NextCloud Draw Toggle               | `true` (when image built with `drawio` plugin)              |                   |
|  `DO_FULLTEXTSEARCH`           | NextCloud Full Text Search Toggle   | `true` (when image built with `fulltextsearch` plugin)      |                   |
|  `DO_GLOBALSITESELECTOR`       | NextCloud GlobalSiteSelector Toggle | `true` (when image built with `globalsiteselector` plugin)  |                   |
|  `DO_GROUPFOLDERS`             | NextCloud GroupFolders Toggle       | `true` (when image built with `groupfolders` plugin)        |                   |
|  `DO_IMPERSONATE`              | NextCloud Impersonate Toggle        | `true` (when image built with `impersonate` plugin)         |                   |
|  `DO_LOOL`                     | NextCloud LibreOfficeOnline Toggle  | `true` (when image built with `richdocuments` plugin)       |                   |
|  `DO_MATOMO`                   | NextCloud Matomo Toggle             | `true` (when image built with `cloud_piwik` plugin)         |                   |
|  `DO_MDED`                     | NextCloud MarkDown Editor Toggle    | `true` (when image built with `files_markdown` plugin)      |                   |
|  `DO_PASSWORDS`                | NextCloud Passwords Manager Toggle  | `true` (when image built with `passwords` plugin)           |                   |
|  `DO_PRIVACY`                  | NextCloud Privacy Toggle            | `true`                                                      |                   |
|  `DO_QUOTAWARNING`             | NextCloud Quotas Warning Toggle     | `true` (when image built with `quota_warning` plugin)       |                   |
|  `DO_REASONS`                  | NextCloud Shows Reasons to use NC   | `true` (patch submitted at nextcloud/server#26035)          |                   |
|  `DO_SOCIALS`                  | NextCloud Shows Social Links        | `true` (patch submitted at nextcloud/server#26035)          |                   |
|  `DRAW_DOMAIN`                 | NextCloud Draw Address              | `embed.diagrams.net`                                        |                   |
|  `ELASTICSEARCH_HOST`          | ElasticSearch Host                  | `elasticsearch`                                             |                   |
|  `ELASTICSEARCH_INDEX`         | ElasticSearch Index                 | `nextcloud`                                                 |                   |
|  `ELASTICSEARCH_PORT`          | ElasticSearch Port                  | `9200`                                                      |                   |
|  `ELASTICSEARCH_PROTO`         | ElasticSearch Proto                 | `http`                                                      |                   |
|  `LLNG_PROTO`                  | LemonLDAP-NG SSO Proto              | `http`                                                      |                   |
|  `LLNG_SSO_DOMAIN`             | LemonLDAP-NG SSO FQDN               | `auth.${OPENLDAP_DOMAIN}`                                   |                   |
|  `LOOKUP_JWT_KEY`              | NextCloud Lookup Server JWT Secret  | `secret`                                                    |                   |
|  `LOOL_ENDPOINT`               | LibreOfficeOnline Endpoint          | undef                                                       |                   |
|  `LOOL_DO_PROXY`               | Configure NextCloud as LOOL Proxy   | undef                                                       |                   |
|  `LOOL_PORT`                   | LibreOfficeOnline Port              | `9980`                                                      |                   |
|  `LOOL_PROTO`                  | LibreOfficeOnline Proto             | `http`                                                      |                   |
|  `LOOL_WS_PROTO`               | LibreOfficeOnline WS Proto          | `ws`                                                        |                   |
|  `MAIL_FROM_USER`              | NextCloud Mails From User           | `nextcloud`                                                 |                   |
|  `MAIL_SENDMAIL_MODE`          | NextCloud Sendmail Mode             | `smtp`                                                      |                   |
|  `MAIL_SMTP_MODE`              | NextCloud SMTP Mode                 | `smtp`                                                      |                   |
|  `MATOMO_SITE_ID`              | Matomo Site ID                      | `1`                                                         |                   |
|  `MATOMO_TRACK_DIR`            | Matomo Track File Browsing          | `true`                                                      |                   |
|  `MATOMO_TRACK_USER`           | Matomo Track User IDs               | `true`                                                      |                   |
|  `MATOMO_URL`                  | Matomo Server URL                   | undef                                                       |                   |
|  `MYSQL_DATABASE`              | MySQL Database Name                 | `nextcloud`                                                 |                   |
|  `MYSQL_HOST`                  | MySQL Database Host                 | `127.0.0.1`                                                 |                   |
|  `MYSQL_PASSWORD`              | MySQL Database Password             | undef                                                       |                   |
|  `MYSQL_PORT`                  | MySQL Database Port                 | `3306`                                                      |                   |
|  `MYSQL_USER`                  | MySQL Database Username             | `nextcloud`                                                 |                   |
|  `NEXTCLOUD_ADMIN_PASSWORD`    | NextCloud Admin Password            | undef                                                       |                   |
|  `NEXTCLOUD_ADMIN_USER`        | NextCloud Admin Username            | undef                                                       |                   |
|  `NEXTCLOUD_CONFIGMAP_NAME`    | NextCloud ConfigMap Name            | `nextcloud-config`                                          |                   |
|  `NEXTCLOUD_CRON_INTERVAL`     | NextCloud Cron Interval             | undef, defaults to web cron                                 |                   |
|  `NEXTCLOUD_DATA_DIR`          | NextCloud Data Directory            | `/var/www/html/data`                                        |                   |
|  `NEXTCLOUD_DATA_LOCATION`     | NextCloud Data Location             | ``, country code, for end-users to know about data location |                   |
|  `NEXTCLOUD_GSS_MASTER`        | NextCloud GlobalSiteSelector Master | undef, defaults to `$APACHE_DOMAIN` if GSS enabled          |                   |
|  `NEXTCLOUD_GSS_USERMAPPING`   | NextCloud GSS User Mapping          | `/user-mapping.json`, used only if SAML not configured      |                   |
|  `NEXTCLOUD_K8S_NAME`          | NextCloud ConfigMap Namespace       | undef                                                       |                   |
|  `NEXTCLOUD_LOOKUP_HOST`       | NextCloud Lookup Server Endpoint    | undef (`lookup.nextcloud.com` hardcoded)                    |                   |
|  `ONLY_TRUST_KUBE_CA`          | Don't trust base image CAs          | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`               | OpenLDAP Base                       | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`     | OpenLDAP Bind DN Prefix             | `cn=nextcloud,ou=services`                                  | opsperator/apache |
|  `OPENLDAP_BIND_PW`            | OpenLDAP Bind Password              | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX`     | OpenLDAP Conf DN Prefix             | `cn=lemonldap,ou=config`                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name                | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_GROUP_FILTER`       | OpenLDAP Group Filter               | `All`                                                       |                   |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address            | undef                                                       | opsperator/apache |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port                  | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                      | `ldap`                                                      | opsperator/apache |
|  `OPENLDAP_USERS_OBJECTCLASS`  | OpenLDAP Users ObjectClass          | `inetOrgPerson`                                             | opsperator/apache |
|  `OTHER_TRUSTED_DOMAINS`       | Other Trusted Domains               | undef                                                       |                   |
|  `PHP_ERRORS_LOG`              | PHP Errors Logs Output              | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`      | PHP Max Execution Time              | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`        | PHP Max File Uploads                | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`           | PHP Max Post Size                   | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`     | PHP Max Upload File Size            | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`            | PHP Memory Limit                    | `-1` (no limitation)                                        | opsperator/php    |
|  `POSTGRES_DB`                 | Postgres Database Name              | `nextcloud`                                                 |                   |
|  `POSTGRES_HOST`               | Postgres Database Host              | `127.0.0.1`                                                 |                   |
|  `POSTGRES_PASSWORD`           | Postgres Database Password          | undef                                                       |                   |
|  `POSTGRES_PORT`               | Postgres Database Port              | `5432`                                                      |                   |
|  `POSTGRES_USER`               | Postgres Database Username          | `nextcloud`                                                 |                   |
|  `PUBLIC_PROTO`                | NextCloud HTTP Proto                | `http`                                                      | opsperator/apache |
|  `REDIS_DB`                    | NextCloud FileLock Redis Database   | `0`                                                         |                   |
|  `REDIS_HOST`                  | NextCloud FileLock Redis Host       | undef (defaults to sgbd based files locking)                |                   |
|  `REDIS_PASSWORD`              | NextCloud FileLock Redis Password   | undef                                                       |                   |
|  `REDIS_PORT`                  | NextCloud FileLock Redis Port       | `6379`                                                      |                   |
|  `S3_ACCESS_KEY`               | S3 ObjectStore Access Key           | undef                                                       |                   |
|  `S3_BUCKET`                   | S3 ObjectStore Bucket               | `nextcloud`                                                 |                   |
|  `S3_HOST`                     | S3 ObjectStore Proxy Host           | undef                                                       |                   |
|  `S3_PORT`                     | S3 ObjectStore Proxy Port           | `8080`                                                      |                   |
|  `S3_REGION`                   | S3 ObjectStore Region               | `us-east-1`                                                 |                   |
|  `S3_SECRET_KEY`               | S3 ObjectStore Secret Key           | undef                                                       |                   |
|  `SAML_ADMIN_USER`             | Username to set as Admin            | `admin0`                                                    |                   |
|  `SAML_LOCKOUT`                | LemonLDAP-NG SSO autoconf           | undef                                                       |                   |
|  `SMTP_HOST`                   | NextCloud SMTP Relay                | undef                                                       |                   |
|  `SMTP_PORT`                   | NextCloud SMTP Port                 | `25`                                                        |                   |
|  `SQLITE_DATABASE`             | SQLite Database File                | undef                                                       |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                             | Inherited From    |
| :------------------ | --------------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)           | opsperator/apache |
|  `/var/www/html`    | NextCloud Runtime directory             |                   |
