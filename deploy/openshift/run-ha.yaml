apiVersion: v1
kind: Template
labels:
  app: nextcloud
  template: nextcloud-statefulset-persistent
message: |-
  The following service(s) have been created in your project:
      https://auth.${ROOT_DOMAIN} -- LLNG Portal
      https://manager.${ROOT_DOMAIN} -- LLNG Manager
      https://cloud.${ROOT_DOMAIN} -- NextCloud

  Global admin username: admin0
  and password: see openldap-${FRONTNAME} secret
metadata:
  annotations:
    description: NextCloud HA
    iconClass: icon-php
    openshift.io/display-name: NextCloud HA
    tags: nextcloud
  name: nextcloud-statefulset-persistent
objects:
- apiVersion: apps/v1
  kind: StatefulSet
  metadata:
    name: openldap-${FRONTNAME}
  spec:
    selector:
      matchLabels:
        name: openldap-${FRONTNAME}
    serviceName: openldap-${FRONTNAME}
    replicas: 3
    template:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: name
                operator: In
                values:
                - openldap-${FRONTNAME}
            topologyKey: kubernetes.io/hostname
      metadata:
        labels:
          name: openldap-${FRONTNAME}
      spec:
        containers:
        - env:
          - name: OPENLDAP_BIND_LDAP_PORT
            value: "1389"
          - name: OPENLDAP_BIND_LDAPS_PORT
            value: "1636"
          - name: OPENLDAP_BLUEMIND_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: bluemind-password
          - name: OPENLDAP_DEBUG_LEVEL
            value: "${OPENLDAP_DEBUG_LEVEL}"
          - name: OPENLDAP_FUSION_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: fusion-password
          - name: OPENLDAP_GLOBAL_ADMIN_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: global-admin-password
          - name: OPENLDAP_HOST_ENDPOINT
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_LEMONLDAP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: lemonldap-password
          - name: OPENLDAP_LEMONLDAP_HTTPS
            value: "yes"
          - name: OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: lemonldap-sessions-password
          - name: OPENLDAP_LEMON_HTTP_PORT
            value: "8080"
          - name: OPENLDAP_MEDIAWIKI_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: mediawiki-password
          - name: OPENLDAP_MONITOR_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: monitor-password
          - name: OPENLDAP_NEXTCLOUD_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: nextcloud-password
          - name: OPENLDAP_ORG_SHORT
            value: "${ORG_NAME}"
          - name: OPENLDAP_ROCKET_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: rocket-password
          - name: OPENLDAP_ROOT_DN_PREFIX
            value: cn=admin
          - name: OPENLDAP_ROOT_DN_SUFFIX
            value: "${BASE_SUFFIX}"
          - name: OPENLDAP_ROOT_DOMAIN
            value: "${ROOT_DOMAIN}"
          - name: OPENLDAP_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: root-password
          - name: OPENLDAP_SMTP_SERVER
            value: "${SMTP_RELAY}"
          - name: OPENLDAP_SSO_CLIENT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: ssoapp-password
          - name: OPENLDAP_SSP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: ssp-password
          - name: OPENLDAP_STATEFULSET_NAME
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_SYNCREPL_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: syncrepl-password
          - name: OPENLDAP_WHITEPAGES_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: whitepages-password
          image: ${OPENSHIFT_REGISTRY}/${BUILDS}/${OPENLDAP_IMAGESTREAM_TAG}
          imagePullPolicy: Always
          livenessProbe:
            initialDelaySeconds: 30
            timeoutSeconds: 1
            tcpSocket:
              port: 1389
          name: openldap
          ports:
          - containerPort: 1389
            protocol: TCP
          - containerPort: 1636
            protocol: TCP
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - "-i"
              - "-c"
              - /usr/local/bin/is-ready.sh
            initialDelaySeconds: 5
            timeoutSeconds: 1
          resources:
            limits:
              cpu: "${OPENLDAP_CPU_LIMIT}"
              memory: "${OPENLDAP_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - name: conf
            mountPath: /etc/openldap
          - name: data
            mountPath: /var/lib/ldap
          - name: run
            mountPath: /run
        volumes:
        - emptyDir: {}
          name: run
    volumeClaimTemplates:
    - metadata:
        name: conf
      spec:
        accessModes: [ ReadWriteOnce ]
        resources:
          requests:
            storage: ${OPENLDAP_CONF_VOLUME_CAPACITY}
    - metadata:
        name: data
      spec:
        accessModes: [ ReadWriteOnce ]
        resources:
          requests:
            storage: ${OPENLDAP_DATA_VOLUME_CAPACITY}
    triggers:
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      template.openshift.io/expose-uri: ldaps://{.spec.clusterIP}:{.spec.ports[?(.name=="ldaps")].port}
    name: openldap-${FRONTNAME}
  spec:
    ports:
    - name: ldap
      protocol: TCP
      port: 1389
      targetPort: 1389
      nodePort: 0
    - name: ldaps
      protocol: TCP
      port: 1636
      targetPort: 1636
      nodePort: 0
    selector:
      name: openldap-${FRONTNAME}
    clusterIP: None
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: lemon-${FRONTNAME}
    name: lemon-${FRONTNAME}
  spec:
    replicas: 3
    selector:
      name: lemon-${FRONTNAME}
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          name: lemon-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        hostAliases:
        - ip: "127.0.0.1"
          hostnames:
          - "auth.${ROOT_DOMAIN}"
          - "manager.${ROOT_DOMAIN}"
          - "reload.${ROOT_DOMAIN}"
        containers:
        - env:
          - name: OPENLDAP_BASE
            value: "${BASE_SUFFIX}"
          - name: OPENLDAP_BIND_DN_PREFIX
            value: cn=lemonldap,ou=services
          - name: OPENLDAP_BIND_PW
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: lemonldap-password
          - name: OPENLDAP_CONF_DN_PREFIX
            value: ou=lemonldap,ou=config
          - name: OPENLDAP_DOMAIN
            value: "${ROOT_DOMAIN}"
          - name: OPENLDAP_HOST
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_HTTP_PORT
            value: "8080"
          - name: OPENLDAP_PORT
            value: "1389"
          - name: OPENLDAP_PROTO
            value: ldap
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 15
            httpGet:
              path: /
              port: 8080
            initialDelaySeconds: 30
            periodSeconds: 20
            timeoutSeconds: 8
          name: lemon
          ports:
          - containerPort: 8080
            protocol: TCP
          readinessProbe:
            httpGet:
              path: /
              port: 8080
            initialDelaySeconds: 5
            periodSeconds: 20
            timeoutSeconds: 5
          resources:
            limits:
              cpu: "${LEMON_CPU_LIMIT}"
              memory: "${LEMON_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - name: apachesites
            mountPath: /etc/apache2/sites-enabled
          - name: etcconf
            mountPath: /etc/lemonldap-ng
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: etcconf
        - emptyDir: {}
          name: apachesites
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - lemon
        from:
          kind: ImageStreamTag
          name: ${LEMON_IMAGESTREAM_TAG}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: lemon-${FRONTNAME}
    annotations:
      description: Exposes and load balances the SSO pods
  spec:
    ports:
    - name: sso
      port: 8080
      targetPort: 8080
    selector:
      name: lemon-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    name: lemon-${FRONTNAME}-reload
  spec:
    host: reload.${ROOT_DOMAIN}
    to:
      kind: Service
      name: lemon-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
- apiVersion: v1
  kind: Route
  metadata:
    name: lemon-${FRONTNAME}-manager
  spec:
    host: manager.${ROOT_DOMAIN}
    to:
      kind: Service
      name: lemon-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
- apiVersion: v1
  kind: Route
  metadata:
    name: lemon-${FRONTNAME}-auth
  spec:
    host: auth.${ROOT_DOMAIN}
    to:
      kind: Service
      name: lemon-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: postgres-${FRONTNAME}
  spec:
    accessModes: [ ReadWriteOnce ]
    resources:
      requests:
        storage: ${NEXTCLOUD_POSTGRES_VOLUME_CAPACITY}
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: postgres-${FRONTNAME}
    name: postgres-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: postgres-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: postgres-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: POSTGRESQL_USER
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: database-user
          - name: POSTGRESQL_PASSWORD
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: database-password
          - name: POSTGRESQL_DATABASE
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: database-name
          - name: POSTGRESQL_MAX_CONNECTIONS
            value: ${NEXTCLOUD_POSTGRES_MAX_CONNECTIONS}
          - name: POSTGRESQL_SHARED_BUFFERS
            value: ${NEXTCLOUD_POSTGRES_SHARED_BUFFERS}
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            exec:
              command:
              - /bin/sh
              - '-i'
              - '-c'
              - pg_isready -h 127.0.0.1 -p 5432
            initialDelaySeconds: 30
            timeoutSeconds: 1
          name: postgres
          ports:
          - containerPort: 5432
            protocol: TCP
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - '-i'
              - '-c'
              - psql -h 127.0.0.1 -U $POSTGRESQL_USER -q -d $POSTGRESQL_DATABASE -c 'SELECT 1'
            initialDelaySeconds: 5
            timeoutSeconds: 1
          resources:
            limits:
              cpu: "${NEXTCLOUD_POSTGRES_CPU_LIMIT}"
              memory: "${NEXTCLOUD_POSTGRES_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - name: data
            mountPath: /var/lib/pgsql/data
        restartPolicy: Always
        volumes:
        - name: data
          persistentVolumeClaim:
            claimName: postgres-${FRONTNAME}
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - postgres
        from:
          kind: ImageStreamTag
          name: ${POSTGRES_IMAGESTREAM_TAG}
          namespace: ${POSTGRES_NAMESPACE}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      template.openshift.io/expose-uri: postgres://{.spec.clusterIP}:{.spec.ports[?(.name=="postgresql")].port}
    name: postgres-${FRONTNAME}
  spec:
    ports:
    - name: postgresql
      protocol: TCP
      port: 5432
      targetPort: 5432
      nodePort: 0
    selector:
      name: postgres-${FRONTNAME}
    type: ClusterIP
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: radosgw-${FRONTNAME}
    name: radosgw-${FRONTNAME}
  spec:
    replicas: 2
    selector:
      name: radosgw-${FRONTNAME}
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          name: radosgw-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: CEPH_DAEMON
            value: rgw
          - name: KEYRING_NAME
            value: ${RADOSGW_KEYRING_NAME}
          - name: RGW_NAME
            value: radosgw
          - name: RGW_CIVETWEB_PORT
            value: "8080"
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            initialDelaySeconds: 30
            timeoutSeconds: 1
            tcpSocket:
              port: 8080
          name: radosgw
          ports:
          - containerPort: 8080
            protocol: TCP
          resources:
            limits:
              cpu: "${RADOSGW_CPU_LIMIT}"
              memory: "${RADOSGW_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - name: conf
            mountPath: /var/lib/ceph/radosgw/${CEPH_CLUSTER_NAME}-rgw.radosgw/keyring
            subPath: ${CEPH_CLUSTER_NAME}.client.radosgw.keyring
          - name: conf
            mountPath: /etc/ceph
        restartPolicy: Always
        volumes:
        - name: conf
          configMap:
            defaultMode: 420
            name: radosgw-${FRONTNAME}-conf
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - radosgw
        from:
          kind: ImageStreamTag
          name: ${RADOSGW_IMAGESTREAM_TAG}
          namespace: ${BUILDS}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      template.openshift.io/expose-uri: http://{.spec.clusterIP}:{.spec.ports[?(.name=="http")].port}
    name: radosgw-${FRONTNAME}
  spec:
    ports:
    - name: http
      protocol: TCP
      port: 8080
      targetPort: 8080
      nodePort: 0
    selector:
      name: radosgw-${FRONTNAME}
    type: ClusterIP
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    name: nextcloud-${FRONTNAME}
  spec:
    replicas: 2
    selector:
      name: nextcloud-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: nextcloud-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: LDAP_BASE
            value: "${BASE_SUFFIX}"
          - name: LDAP_BIND_PW
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: nextcloud-password
          - name: OPENLDAP_HOST
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_PORT
            value: "1389"
          - name: LLNG_PROTO
            value: https
          - name: LLNG_SSO_DOMAIN
            value: auth.${ROOT_DOMAIN}
          - name: NEXTCLOUD_ADMIN_PASSWORD
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: admin-password
          - name: NEXTCLOUD_ADMIN_USER
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: admin-user
          - name: NEXTCLOUD_CONFIGMAP_NAME
            value: nextcloud-${FRONTNAME}-conf
          - name: APACHE_DOMAIN
            value: "cloud.${ROOT_DOMAIN}"
          - name: OTHER_TRUSTED_DOMAINS
            value: "nextcloud-${FRONTNAME}"
          - name: POSTGRES_DB
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: database-name
          - name: POSTGRES_HOST
            value: postgres-${FRONTNAME}
          - name: POSTGRES_PASSWORD
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: database-password
          - name: POSTGRES_USER
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: database-user
          - name: S3_ACCESS_KEY
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: s3-access-key
          - name: S3_BUCKET
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: s3-bucket
          - name: S3_HOST
            value: radosgw-${FRONTNAME}
          - name: S3_PORT
            value: "8080"
          - name: S3_SECRET_KEY
            valueFrom:
              secretKeyRef:
                name: nextcloud-${FRONTNAME}
                key: s3-secret-key
          - name: SAML_ADMIN_USER
            value: admin0
          - name: SAML_LOCKOUT
            value: pleasedo
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 5
            initialDelaySeconds: 60
            periodSeconds: 60
            successThreshold: 1
            tcpSocket:
              port: 8080
            timeoutSeconds: 3
          name: nextcloud
          ports:
          - containerPort: 8080
            protocol: TCP
          readinessProbe:
            failureThreshold: 5
            initialDelaySeconds: 60
            periodSeconds: 60
            successThreshold: 1
            tcpSocket:
              port: 8080
            timeoutSeconds: 3
          resources:
            limits:
              cpu: "${NEXTCLOUD_CPU_LIMIT}"
              memory: "${NEXTCLOUD_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - name: apachesites
            mountPath: /etc/apache2/sites-enabled
          - name: conf
            mountPath: /var/k8s/config.php
            subPath: config.php
          - name: data
            mountPath: /var/www/html
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: data
        - configMap:
            defaultMode: 420
            name: nextcloud-${FRONTNAME}-conf
          name: conf
        - emptyDir: {}
          name: apachesites
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - nextcloud
        from:
          kind: ImageStreamTag
          name: nextcloud:${NEXTCLOUD_IMAGE_TAG}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: nextcloud-${FRONTNAME}
    annotations:
      description: Exposes NextCloud
  spec:
    ports:
    - name: cloud
      port: 8080
      targetPort: 8080
    selector:
      name: nextcloud-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    annotations:
      haproxy.router.openshift.io/timeout: 3m
    name: nextcloud-${FRONTNAME}
  spec:
    host: cloud.${ROOT_DOMAIN}
    to:
      kind: Service
      name: nextcloud-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
parameters:
- name: CEPH_CLUSTER_NAME
  description: Ceph Cluster Name
  displayName: Ceph Cluster Name
  value: ceph
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: LEMON_CPU_LIMIT
  description: Maximum amount of CPU a Lemon container can use
  displayName: Lemon CPU Limit
  required: true
  value: 300m
- name: LEMON_IMAGESTREAM_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: lemon:master
- name: LEMON_MEMORY_LIMIT
  description: Maximum amount of memory a Lemon container can use
  displayName: Lemon Memory Limit
  required: true
  value: 512Mi
- name: OPENLDAP_CONF_VOLUME_CAPACITY
  description: Volume space available for OpenLDAP configuration, e.g. 512Mi, 2Gi.
  displayName: OpenLDAP Config Volume Capacity
  required: true
  value: 512Mi
- name: OPENLDAP_CPU_LIMIT
  description: Maximum amount of CPU an OpenLDAP container can use
  displayName: OpenLDAP CPU Limit
  required: true
  value: 300m
- name: OPENLDAP_DATA_VOLUME_CAPACITY
  description: Volume space available for OpenLDAP database, e.g. 512Mi, 2Gi.
  displayName: OpenLDAP Data Volume Capacity
  required: true
  value: 8Gi
- name: OPENLDAP_DEBUG_LEVEL
  description: OpenLDAP log level
  displayName: LDAP Log Level
  required: true
  value: '256'
- name: OPENLDAP_IMAGESTREAM_TAG
  description: OpenLDAP Image Tag
  displayName: OpenLDAP ImageStream Tag
  required: true
  value: openldap:master
- name: OPENLDAP_MEMORY_LIMIT
  description: Maximum amount of memory an OpenLDAP container can use
  displayName: OpenLDAP Memory Limit
  required: true
  value: 512Mi
- name: OPENSHIFT_REGISTRY
  description: OpenShift Registry
  displayName: Registry Address
  required: true
  value: "docker-registry.default.svc:5000"
- name: NEXTCLOUD_CPU_LIMIT
  description: Maximum amount of CPU a NextCloud container can use
  displayName: NextCloud CPU Limit
  required: true
  value: 300m
- name: NEXTCLOUD_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: 17buster
- name: NEXTCLOUD_MEMORY_LIMIT
  description: Maximum amount of memory a NextCloud container can use
  displayName: NextCloud Memory Limit
  required: true
  value: 512Mi
- name: NEXTCLOUD_POSTGRES_CPU_LIMIT
  description: Maximum amount of CPU a NextCloud database container can use
  displayName: NextCloud Postgres CPU Limit
  required: true
  value: 500m
- name: NEXTCLOUD_POSTGRES_MEMORY_LIMIT
  description: Maximum amount of memory a NextCloud database container can use
  displayName: NextCloud Postgres Memory Limit
  required: true
  value: 768Mi
- name: NEXTCLOUD_POSTGRES_MAX_CONNECTIONS
  description: Maximum amount of connections PostgreSQL should accept
  displayName: Maximum Postgres Connections
  required: true
  value: "100"
- name: NEXTCLOUD_POSTGRES_SHARED_BUFFERS
  displayName: Postgres Shared Buffer Amount
  description: Amount of Memory PostgreSQL should dedicate to Shared Buffers
  required: true
  value: 12MB
- name: NEXTCLOUD_POSTGRES_VOLUME_CAPACITY
  description: Volume space available for NextCloud Postgres database, e.g. 512Mi, 2Gi.
  displayName: NextCloud Postgres Volume Capacity
  required: true
  value: 2Gi
- name: POSTGRES_IMAGESTREAM_TAG
  description: PostgreSQL ImageStream Tag
  displayName: postgresql imagestream tag
  required: true
  value: postgresql:10
- name: POSTGRES_NAMESPACE
  description: The OpenShift Namespace where the Postgres ImageStream resides
  displayName: Postgres Namespace
  required: true
  value: openshift
- name: RADOSGW_CPU_LIMIT
  description: Maximum amount of CPU an RadosGW container can use
  displayName: RadosGW CPU Limit
  required: true
  value: 200m
- name: RADOSGW_IMAGESTREAM_TAG
  description: RadosGW ImageStream Tag
  displayName: Ceph RGW imagestream tag
  required: true
  value: radosgw:master
- name: RADOSGW_KEYRING_NAME
  description: RadosGW Keyring Name
  displayName: RadosGW Keyring Name
  required: true
- name: RADOSGW_MEMORY_LIMIT
  description: Maximum amount of memory an RadosGW container can use
  displayName: RadosGW Memory Limit
  required: true
  value: 512Mi
- name: BASE_SUFFIX
  description: OpenLDAP base suffix
  displayName: LDAP Base Suffix
  required: true
  value: dc=demo,dc=local
- name: BUILDS
  description: CI Namespace
  displayName: Builds
  required: true
  value: ci
- name: ORG_NAME
  description: Organization Display Name
  displayName: Organization Display Name
  required: true
  value: Demo
- name: ROOT_DOMAIN
  description: Root Domain
  displayName: Root Domain
  required: true
  value: demo.local
- name: SMTP_RELAY
  description: SMTP Relay
  displayName: SMTP Relay
  required: true
  value: smtp.demo.local
